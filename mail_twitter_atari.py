import socket
import twython
import smtplib
import imaplib
import sys
import unicodedata
from email.header import decode_header
#############################################################################################################
HOST = '0.0.0.0'        # Server ip
PORT = 8000             # Arbitrary non-privileged port

#!si potrebbe chiedere all'utente se vuole cambiare porta, questo è un vantaggio perchè una terza persona dovrebbe fare lo scaning di tutte le property
#questo se si usa una rete con firewall di cui si conosce la porta aperta è comodo oltretutto, a meno che non ci connettiamo direttamente ed allora il programma
#va ripensato per una connessione seriale!!!
#############################################################################################################
my_email_username = 'username' #valido anche per imap
gmail_domain = '@gmail.com'
my_email_address = my_email_username + gmail_domain
my_email_password = 'password' #passwd valida anche per imap
imap_host = 'imap.gmail.com'
imap_user = my_email_username+gmail_domain

imap = imaplib.IMAP4_SSL(imap_host)
imap.login(my_email_address, my_email_password)
imap.select('Inbox')

_, unseen_mail = imap.search(None, 'UNSEEN')

unseen_mail = unseen_mail[0].split()
print(str(len(unseen_mail))+' new messages ')

def strip_accents(s):
    """
    Sanitarize the given unicode string and remove all special/localized
    characters from it.

    Category "Mn" stands for Nonspacing_Mark
    """
    try:
        return ''.join(
            c for c in unicodedata.normalize('NFD', s)
            if unicodedata.category(c) != 'Mn'
        )
    except:
        return s


for mail_id in unseen_mail:
    raw_mail = imap.fetch(mail_id, '(BODY.PEEK[HEADER.FIELDS (SUBJECT FROM)])')[1][0][1].split(b'\r\n')
    for item in raw_mail:
        if item[0:4] == b'From':
            mail_from = item[6:]
        elif item[0:7] == b'Subject':
            mail_subject = item[9:]
        #print(item)  print di debug
    a = decode_header(mail_subject.decode())[0][0]
    b = decode_header(mail_from.decode())[0][0]

    if isinstance(a, bytes):
        a = a.decode()
    if isinstance(b, bytes):
        b = b.decode()
    #print(raw_mail) print di debug
    print('\n')
    print(mail_id.decode() + ': ' + strip_accents(a))
    print(b)


sys.exit()
##############################################################################################################
#Consumer API keys
consumer_key = "INSERT TOKEN HERE" #(API key)
consumer_secret = "INSERT TOKEN HERE" #(API secret key)

#Access token & access token secret
access_token = "INSERT TOKEN HERE" #(Access token)
access_token_secret = "INSERT TOKEN HERE" #(Access token secret)
##############################################################################################################

def file_write(nome_file, testo): # Scrive un file.
    out_file = open( nome_file ,"w")
    out_file.write(testo)
    out_file.close()

def file_read(nome_file): # Legge un file.
    in_file = open(nome_file,"r")
    text = in_file.read()
    in_file.close()
    print('\n')
    print(text)


def send_email(recipient, subject, message):
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.ehlo()
    server.starttls()
    server.login(my_email_username, my_email_password)
    server.sendmail(my_email_address, recipient, 'Subject:' + subject + '\n' + message)

def send_message(conn, message):
    conn.send((message+'\r\n').encode('utf8'))


def receive_string(conn):
    message = []
    while 1:
        try:
            data = conn.recv(10).decode('utf8')
            if not data:
                break
            elif data == "\r":
                #conn.send(b"connessione chiusa\r\n")
                break
            message.append(data)
        except KeyboardInterrupt:
            print('abort')
            break
    print(message)
    decoded_msg = "".join(message)
    print(decoded_msg)
    return decoded_msg


twitter = twython.Twython(consumer_key,consumer_secret,access_token,access_token_secret)


def create_socket():
    s = socket.socket(socket.AF_INET , socket.SOCK_STREAM)
    s.bind((HOST,PORT))
    print('Server running', HOST, PORT)
    s.listen(1)
    return s

s = create_socket()

should_terminate_server = False

while should_terminate_server == False: #loop per la chiususra del demone

    conn, addr = s.accept()
    print('Connected by', addr)
    print('\n')

    #conn.send(b"connessione aperta\r\n")  provo a sostituirlo con la funzione
    send_message(conn, 'connessione aperta')
    print('connessione aperta')

    should_terminate_connection = False
    while should_terminate_connection == False: #loop per la chiusura della connessione
        send_message(conn, "Inserire comando:\r\n0: tweet  -  1: send email\r\n2: cambiare porta\r\ndisconnect: terminate connection\r\nkill: terminate server")
        response = receive_string(conn)


        if response == '0': #parte per il tweettare
            send_message(conn, 'Insert tweet message content')
            tweet_msg = receive_string(conn)
            if len(tweet_msg) == 0:
                print('Error receiving twitter message')
                send_message(conn, 'Error receiving twitter message')
            elif len(tweet_msg) > 240:
                send_message(conn, 'Tweet longer than 240 chars... truncating')
            twitter.update_status(status = tweet_msg[0:240])
        elif response == '1': #parte per mandare una mail
            must_confirm = True
            while must_confirm:
                #print('passare i parametri per creare il file')
                #nome = input('nome: ')
                send_message(conn, 'passare i parametri per creare il file [nome.estensione]: ')
                file_name = receive_string(conn)
                #estensione = input('estensione: ')
                #print('\n')
                send_message(conn, 'Insert recipient')
                recipient = receive_string(conn)
                send_message(conn, 'Insert subject')
                subject = receive_string(conn)
                send_message(conn, 'Insert body')
                body = receive_string(conn)
                send_message(conn, 'Sending email to ' + recipient + ' confirm with yes/no or abort')
                ack = receive_string(conn)
                testo = recipient+'\n'+'subject: '+subject+'\n'+'boby: '+body+'\n'+ack+'\n'
                file_write(file_name, testo)
                if ack == 'yes' or ack == 'y':
                    must_confirm = False
                    send_email(recipient, subject, body)
                elif ack == 'no' or ack == 'n':
                    must_confirm = True
                elif ack == 'abort':
                    must_confirm = False
                else:
                    send_message('wrong choice')
                    must_confirm = True
        elif response == '2':
            send_message(conn, 'inserire la nuova porta: ')
            PORT = int(receive_string(conn))
            print('la porta verrà cambiata in: ', PORT)
            s.close()
            s = create_socket()
            print('Server re-running', HOST, PORT)
            #should_terminate_connection = True
        elif response == 'disconnect' or response == 'disc':
            should_terminate_connection = True
        elif response == 'kill':
            print('Goodbye cruel world')
            send_message(conn, 'Killing server - farewell')
            should_terminate_server = True
            should_terminate_connection = True
        else:
            send_message(conn, 'Invalid command')
            print('invalid command')

    print('Closing connection - bye')
    send_message(conn, 'Closing connection - bye')
    conn.close()
